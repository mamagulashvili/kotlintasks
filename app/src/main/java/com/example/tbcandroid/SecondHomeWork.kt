package com.example.tbcandroid

fun main() {
    println(numberConverterToText(627))
}

fun numberConverterToText(number: Int): String {
    var result = ""
    val numberList = mutableListOf<String>()
    number.toString().forEach { numberList.add(it.toString()) }

    val mapOfHundreds = mutableMapOf(
        "1" to "ას",
        "2" to "ორას",
        "3" to "სამას",
        "4" to "ოთხას",
        "5" to "ხუთას",
        "6" to "ექვსას",
        "7" to "შვიდას",
        "8" to "რვაას",
        "9" to "ცხრაას"
    )
    val mapOfUnits = mutableMapOf(
        "1" to "ერთი",
        "2" to "ორი",
        "3" to "სამი",
        "4" to "ოთხი",
        "5" to "ხუთი",
        "6" to "ექვსი",
        "7" to "შვიდი",
        "8" to "რვა",
        "9" to "ცხრა"
    )
    val mapOfTens = mutableMapOf(
        "1" to "ათი",
        "2" to "ოცი",
        "3" to "ოცდაათი",
        "4" to "ორმოცი",
        "5" to "ორმოცდაათი",
        "6" to "სამოცი",
        "7" to "სამოცდაათი",
        "8" to "ოთხმოცი",
        "9" to "ოთხმოცდაათი"
    )
    val mapOfPluralNumber = mutableMapOf(
        "1" to "თერთმეტი",
        "2" to "თორმეტი",
        "3" to "ცამეტი",
        "4" to "თოთხმეტი",
        "5" to "თხუთმეტი",
        "6" to "თექვსმეტი",
        "7" to "ჩვიდმეტი",
        "8" to "თვრამეტი",
        "9" to "ცხრამეტი"
    )
    val mapOfPluralTens = mutableMapOf<String, String>(
        "1" to "თერთმეტი",
        "2" to "ოცდა",
        "3" to "ოცდა",
        "4" to "ორმოცდა",
        "5" to "ორმოცდა",
        "6" to "სამოცდა",
        "7" to "სამოცდა",
        "8" to "ოთხმოცდა",
        "9" to "ოთხმოცდა"
    )
    when (numberList.size) {
        3 -> {
            if (numberList[1] == "0" && numberList[2] == "0") {
                result = "${mapOfHundreds[numberList[0]]}ი"
            } else if (numberList[1] == "0") {
                result = "${mapOfHundreds[numberList[0]]}${mapOfUnits[numberList[2]]}"
            } else if (numberList[2] == "0") {
                result = "${mapOfHundreds[numberList[0]]}${mapOfTens[numberList[1]]}"
            } else {

                when (numberList[1].toInt() % 2) {
                    0 -> {
                        result =
                            "${mapOfHundreds[numberList[0]]}${mapOfPluralTens[numberList[1]]}${mapOfUnits[numberList[2]]}"
                    }
                    1 -> {
                        if (numberList[1] == "1") {
                            result =
                                "${mapOfHundreds[numberList[0]]}${mapOfPluralNumber[numberList[2]]}"
                        } else {
                            result =
                                "${mapOfHundreds[numberList[0]]}${mapOfPluralTens[numberList[1]]}${mapOfPluralNumber[numberList[2]]}"
                        }
                    }
                }
            }
        }
        2 -> {
            if (numberList[1] == "0") {
                result = "${mapOfTens[numberList[0]]}"
            } else {
                when (numberList[0].toInt() % 2) {
                    0 -> {
                        result =
                            "${mapOfPluralTens[numberList[0]]}${mapOfUnits[numberList[1]]}"
                    }
                    1 -> {
                        if (numberList[1] == "1") {
                            result =
                                "${mapOfPluralNumber[numberList[1]]}"
                        } else {
                            result =
                                "${mapOfPluralTens[numberList[0]]}${mapOfPluralNumber[numberList[1]]}"
                        }
                    }
                }
            }
        }
        1 -> {
            result = if (numberList[0] == "0"){
                "ნული"
            }else{
                "${mapOfUnits[numberList[0]]}"
            }
        }
        4 -> {
           result = "ათასი"
        }
    }

    return result
}