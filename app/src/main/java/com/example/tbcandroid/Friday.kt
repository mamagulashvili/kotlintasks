package com.example.tbcandroid

fun main() {
    val firstList = mutableListOf( 2, 2,2, 3, 4, 56, 9, 87,87, 5, 4, 4)
    val secondList = mutableListOf(9, 34, 66, 7, 23, 88, 3, 5)
    println(fifthTask(firstList))
}

fun firstTask(list: MutableList<Int>): Int {
    val result = list.toSet()
    return result.size

}

fun secondTask(firstList: MutableList<Int>, secondList: MutableList<Int>): MutableList<Int> {
    var result = mutableListOf<Int>()
    firstList.forEach { if (secondList.contains(it)) result.add(it) }
    return result
}

fun thirdTask(firstList: MutableList<Int>, secondList: MutableList<Int>): MutableList<Int> {
    secondList.forEach { firstList.add(it) }
    return firstList
}

fun fourthTask(list: MutableList<Int>): MutableList<Int> {
    var sum = 0
    var average = 0
    val result = mutableListOf<Int>()

    list.forEach { sum += it }

    average = sum / list.size
    var index = 0
    for (i in 0 until average) {
        if (index == list.size) {
            index = 0
        }
        result.add(list[index])
        index++
    }
    return result
}

fun fifthTask(list: MutableList<Int>): String {
    val min = list.minOrNull()
    val max = list.maxOrNull()
    while (list.contains(min)){
        list.remove(min)
    }
    while (list.contains(max)){
        list.remove(max)
    }

    val resultMin = list.minOrNull()
    val resultMax = list.maxOrNull()

    return "მეორე მაქსიმალური $resultMax , მეორე მინიმალური $resultMin"
}