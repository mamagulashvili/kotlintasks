package com.example.tbcandroid

import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

fun main() {
    val math = MathOperations()

    /*
    val result:Int = math.evenNumberSumCounter()
    println(result)
     */
    /*
    println(math.greatestCommonDivisor(48,72))
     */

//    println(math.leastCommonMultiple(-105,-350))

//    println(math.containsSymbol("giorgi mamagulashvlli\$tbc'$' android"))

//    println(math.reverseNumber(10220))

    println(math.isPalindrome("AnNa"))

}

class MathOperations {
    fun isPalindrome(text: String):Boolean{
        var result = false
        val palindromeText = text.toLowerCase(Locale.ROOT).reversed()
        if (palindromeText == text.toLowerCase(Locale.ROOT)){
            result = true
        }
        return result
    }

    fun reverseNumber(number: Int) :Int{
        var numb = number
        var result = 0
        for (i in 1..numb.toString().length) {
            val remainder = numb % 10
            result = result * 10 + remainder
            numb /=10
        }
        return result
    }

    fun containsSymbol(text: String): Boolean {
        var result = false
        if (text.contains("$")) {
            result = true
        }
        return result
    }

    fun leastCommonMultiple(firstNumber: Int, secondNumber: Int): Int {
        var lcm: Int = 0
        var multiplier = 1
        val firstNumberMultiplesList = ArrayList<Int>()
        val secondNumberMultiplesList = ArrayList<Int>()
        val sameMultiplesList = ArrayList<Int>()

        for (i in 0 until maxOf(abs(firstNumber), abs(secondNumber))) {
            val resultOne = abs(firstNumber) * multiplier
            val resultTwo = abs(secondNumber) * multiplier
            multiplier++
            firstNumberMultiplesList.add(resultOne)
            secondNumberMultiplesList.add(resultTwo)
        }
        firstNumberMultiplesList.forEach {
            if (secondNumberMultiplesList.contains(it)) sameMultiplesList.add(
                it
            )
        }

        println(firstNumberMultiplesList)
        println(secondNumberMultiplesList)
        println(sameMultiplesList)
        lcm = sameMultiplesList.minOrNull()!!
        return lcm

    }

    fun evenNumberSumCounter(lastEvenNumber: Int = 100): Int {
        return if (lastEvenNumber == 0)
            lastEvenNumber
        else
            lastEvenNumber + evenNumberSumCounter(lastEvenNumber - 2)
    }

    fun greatestCommonDivisor(firstNumber: Int, secondNumber: Int): Int {
        val gcd: Int

        val firstNumberDivisorList = ArrayList<Int>()
        val secondNumberDivisorList = ArrayList<Int>()
        val firstNum = abs(firstNumber)
        val secondNum = abs(secondNumber)

        for (i in firstNum downTo 1) {
            if (firstNumber % i == 0) {
                val result = firstNumber / i
                firstNumberDivisorList.add(result)
            }
        }
        for (i in secondNum downTo 1) {
            if (secondNumber % i == 0) {
                val result = secondNumber / i
                secondNumberDivisorList.add(result)
            }
        }
        val sameDivisorArrayList = ArrayList<Int>()
        firstNumberDivisorList.forEach {
            if (secondNumberDivisorList.contains(it)) sameDivisorArrayList.add(
                it
            )
        }

        println(sameDivisorArrayList)
        println(firstNumberDivisorList)
        println(secondNumberDivisorList)

        gcd = sameDivisorArrayList.maxOrNull()!!

        return gcd
    }

}